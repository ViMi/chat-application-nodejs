const PORT = process.env.PORT || 3000;
//--------------

const express = require("express");
const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);
app.use(express.static(__dirname + "/../public"));
const  {
      addUser
    , getUserByName
    , removeUser
    , getUser
    , getUsersInRoom
        } = require("./utils/users");

const {generateMessage} = require("./utils/messages");


//If we have index.html it is will loaded automaticaly
//without app.get function call
/*
app.get("/", (req,res) => {
    res.sendFile(__dirname + "/index.html");
});
 */

io.on('connection', socket => {

    socket.on("join", (options, callback) => {

        // ... Syntax link object literal to object literal in below context
        const {error, user} = addUser( { id: socket.id, ...options } );

        if( error ) {
            return callback(error);
        }

        socket.join( user.room );
        socket.emit("message", generateMessage("Admin", `Welcome ,${ user.username }`));
        socket.broadcast.to( user.room ).emit("dataRoom", getUsersInRoom(user.room));
        socket.emit("dataRoom", getUsersInRoom( user.room ));
        socket.broadcast.to( user.room ).emit("message", generateMessage("Admin", `${ user.username } has joined`));
        callback();
    });


    socket.on("sendPrivate", options => {

        let msg = generateMessage(options.username + " [PRIVATE]", options.msg);

        socket.to(getUserByName(options.toUser).id).emit("message", msg);
        socket.emit("message",msg);

    });


    socket.on("dataRoom", room => {
        socket.emit("dataRoom", _ => getUsersInRoom(room.room));
    });


    socket.on("messageTape", options => {
        socket.broadcast.to( options.room ).emit("messageTape", generateMessage(options.username,`is Taping`));
    });

    socket.on("messageTapeStop", options => {
        socket.broadcast.to( options.room ).emit("messageTapeStop", options);
    });


    socket.on("sendMessage", options => {
        let msg = generateMessage(options.username, options.msg);
        socket.emit("message", msg);
        socket.broadcast.to(options.room.trim()).emit("message", msg);
    });


    socket.on("disconnect", _ => {
        let user = getUser(socket.id);
        if(user) {
            let msg = generateMessage("Admin", `User ${user.username} go home `);
            socket.broadcast.to(user.room).emit("message", msg);

            removeUser(socket.id);
            socket.broadcast.to(user.room).emit("dataRoom", getUsersInRoom(user.room));
        }
    });

});


http.listen(PORT, _ => console.log("listening on 3000") );
