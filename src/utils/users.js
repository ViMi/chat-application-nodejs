//In real work we of course must store users in external data
//But now we store it in Server Main Memory
const users = []


//Add User To The Room
//Id it is a Socket.io identifier
const addUser = ( { id, username, room } ) => {
    // Clean the data
    username = username.trim().toLowerCase()
    room = room.trim().toLowerCase()

    // Validate the data
    if ( !username || !room ) {
        return {
            error: 'Username and room are required!'
        }
    }

    // Check for existing user
    const existingUser = users.find(user => {
        return user.room === room && user.username === username
    })

    // Validate username
    if ( existingUser ) {
        return {
            error: 'Username is in use!'
        }
    }

    // Store user
    const user = { id, username, room }
    users.push( user )
    return { user }
}



//Remove User From Room
const removeUser = id => {
    const index = users.findIndex((user) => user.id === id)

    if ( index !== -1 ) {
        return users.splice(index, 1)[0]
    }
}



const getUser = id => users.find((user) => user.id === id);
const getUserByName = name => users.find( user => user.username === name);


const getUsersInRoom = room => {
    room = room.trim().toLowerCase()
    return users.filter((user) => user.room === room)
}



module.exports = {
    addUser,
    getUserByName,
    removeUser,
    getUser,
    getUsersInRoom
}