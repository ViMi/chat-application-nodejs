sessionStorage.setItem("status", "active");

//If variable point to html elements names must began with $
//Standard Rule
//-->elements
const $messageForm = document.querySelector("#message-form");
const $messageFormInput = $messageForm.querySelector("input");
const $messageFormButton = document.querySelector("button");
const $messages = document.querySelector("#messages");
const $sidebar = document.getElementById("sidebar");


//-->Templates
const messageTemplate = document.querySelector("#message-template").innerHTML;
const sidebarTemplate = document.querySelector('#sidebar-template').innerHTML;

//
let $userTape = null;

let socket = io();

//Get data from URL by qs library (qs allows to parse string)
//Qs start with capital letter because it is a pointer to the object
//from the library...
//URL data include ? prefix and we must remove it to parse to object by Qs
const { username, room } = Qs.parse( location.search,
    {
        ignoreQueryPrefix: true
    }
    );

sessionStorage.setItem("username", username);
sessionStorage.setItem("room", room);


socket.emit("join", { username, room }, error => {
    if( error ) {
        console.log(error);
        location.href = "/"; //Redirection to enter page (index.html)
    }
});


socket.on("dataRoom", roomUsers => displayRoomData( roomUsers ) );
socket.on("messageTapeStop", user => removeTapeMessage( user ) );
socket.on("messageTape", msg => displayTapeMessage( msg ) )
socket.on("message", msg =>  displayMessage( msg ) );



window.onclose = _ => socket.emit("disconnect");

$messageForm.addEventListener("submit", e => {
    e.preventDefault();
    let msg = $messageFormInput.value;
    if ( !msg ) {
        return false;
    }


    socket.emit("sendMessage", { username, room, msg });
});


$messageFormInput.addEventListener("input",
        _ => socket.emit("messageTape", { username, room })
);


$messageFormInput.addEventListener("change",
        _ => socket.emit("messageTapeStop", { room, username })
);
//--------------------------

function displayMessage( message ) {

    let bgClass =
        message.username === "Admin" ? "message message-back-admin" :
            message.username === username ? "message message-back-user"
                : "message message-back-participant";


    const html = Mustache.render( messageTemplate, {
        username : message.username,
        message : message.text,
        createdAt: moment(message.createdAt).format('h:mm'),
        bgClass: bgClass
    });

    $messages.insertAdjacentHTML('beforeend', html);
    $messages.scrollTop = $messages.scrollHeight;
}



function displayRoomData( roomUsers ) {
    $sidebar.innerHTML = "";

    const html = Mustache.render( sidebarTemplate, {
        room : room,
        currentUser : username,
        users : roomUsers
    });


    $sidebar.insertAdjacentHTML('beforeend', html);

    let $usersInRoom = $sidebar.getElementsByClassName('list-group-item');
    for(let i = 0; i < $usersInRoom.length; ++i) {
        $usersInRoom[i].addEventListener("click", e => {

            let msg = $messageFormInput.value;
            let toUser = e.target.textContent.trim();

            toUser !== username ?
                socket.emit("sendPrivate", {username, msg, toUser})
                : false;

        });
    }


}



function displayTapeMessage( message ) {

    if( !$userTape ) {

        let bgClass = "message message-back-typing " + message.username;

        $userTape = Mustache.render(messageTemplate, {
            username: message.username,
            message: message.text,
            bgClass: bgClass
        });

        $messages.insertAdjacentHTML("beforeend", $userTape);
    }
}


function removeTapeMessage( user ) {

    if( $userTape ) {
        let msg = document.getElementsByClassName(user.username)[0];
        msg.parentNode.removeChild(msg);
        $userTape = null;
    }
}